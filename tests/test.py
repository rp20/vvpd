import inspect
import os
import sys
import unittest


cur_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
src_path = os.path.join(os.path.dirname(cur_dir), 'src')
sys.path.insert(0, src_path)


from function import *


class FunctionTest(unittest.TestCase):
    def test_exp_zero_value(self):
        test_value = exp(0, 0.001)
        self.assertEqual(test_value, 1)

    def test_sin_zero_value(self):
        test_value = sin(0, 0.001)
        self.assertEqual(test_value, 0)

    def test_cos_zero_value(self):
        test_value = cos(0, 0.001)
        self.assertEqual(test_value, 1)

    def test_ln_zero_value(self):
        test_value = ln(0, 0.001)
        self.assertEqual(test_value, 0)

    def test_polynomial_zero_value(self):
        test_value = polynomial(0, 0.001, 2)
        self.assertEqual(test_value, 1)

    def test_arctg_zero_value(self):
        test_value = arctg(0, 0.001)
        self.assertEqual(test_value, 0)


if __name__ == '__main__':
    unittest.main()
