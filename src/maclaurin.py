
import PySimpleGUI as sg

from function import *


def maclaurin_gui():
    """
        Графический интерфейс приложения
    """
    sg.theme('DarkAmber')

    input_values = [[sg.Text('Enter e'), sg.InputText()],
                    [sg.Text('Enter x'), sg.InputText()],
                    [sg.Text('Enter a'), sg.InputText()],
                    [sg.Button('Calculate')]]
    equations = ['e^x', 'sin(x)', 'cos(x)', 'ln(1+x)', '(1+x)^a', 'arctg(x)']
    layout = [
        [sg.Listbox(select_mode='LISTBOX_SELECT_MODE_SINGLE', values=equations, size=(10, 7)), sg.Column(input_values)],
        [sg.Text('Result'), sg.Output(size=(40, 2))],
        [sg.Button('Cancel')]
    ]

    window = sg.Window('Window Title', layout)

    while True:
        event, values = window.read()

        if event == sg.WIN_CLOSED or event == 'Cancel':
            break

        if event == 'Calculate':
            if values[0] and values[1] and values[2] and \
                    values[1].replace('.', '', 1).isdigit() and \
                    values[2].replace('.', '', 1).isdigit():

                e = float(values[1])
                x = float(values[2])

                if equations[0] in values[0]:
                    print(exp(x, e))

                elif equations[1] in values[0]:
                    print(sin(x, e))

                elif equations[2] in values[0]:
                    print(cos(x, e))

                elif equations[3] in values[0]:
                    print(ln(x, e))

                elif equations[4] in values[0]:
                    if values[3] and values[3].replace('.', '', 1).isdigit():
                        a = float(values[3])
                        print(polynomial(x, e, a))
                    else:
                        print('Enter a')

                elif equations[5] in values[0]:
                    print(arctg(x, e))
            else:
                print('choose the equation and enter values')

    window.close()


if __name__ == "__main__":
    maclaurin_gui()
