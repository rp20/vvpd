# coding=utf-8
def check(val, max_val=0, min_val=0, type_val=float):
    """
    Функция проверяющая соответсвие числа val, переданным в нее условиям
    """
    if max_val != 0:
        if val > max_val:
            return False

    if min_val != 0:
        if val < min_val:
            return False

    if type(val) != type_val:
        if (type_val == float or type_val == int) and (type(val) == int or type(val) == float):
            return True
        else:
            return False
    else:
        return True


def exp(x, e):
    """
    Функция высчитавающая экспоненту в степени x с точностью e
    """

    if check(x) and check(e):
        result = 1
        current_member = 2
        cur_n = 1
        while current_member > e:
            current_member = pow(x, cur_n) / factorial(cur_n)
            result += current_member
            cur_n += 1

        return result
    else:
        return 'Incorrect values!'


def pow(x, n):
    """
    Функция возводящая x в степень n
    """
    result = x
    if n > 0:
        for i in range(1, n):
            result = result * x
        return result
    elif n == 0:
        return 1


def factorial(x):
    """
    Функция высчитывающая факториал числа x
    """
    result = 1
    for i in range(1, x + 1):
        result *= i
    return result


def sin(x, e):
    """
    Функция высчитывающая синус числа x с точностью e
    """
    if check(x) and check(e):
        result = 0
        current_member = 2
        current_n = 1
        while current_member > e:
            numerator = pow(x, (current_n * 2 - 1))
            denominator = factorial(2 * current_n - 1)
            current_member = numerator / denominator
            result += current_member * pow(-1, current_n + 1)
            current_n += 1

        return result
    else:
        return 'Incorrect values!'


def cos(x, e):
    """
    Функция высчитывающая косинус числа x с точностью e
    """
    if check(x) and check(e):
        result = 0
        current_n = 0
        current_member = 2

        while current_member > e:
            if current_n == 0:
                current_member = 1
            else:
                numerator = pow(x, 2 * current_n)
                denominator = factorial(2 * current_n)
                current_member = numerator / denominator

            result += current_member * pow(-1, current_n)
            current_n += 1

        return result
    else:
        return 'Incorrect values!'


def ln(x, e):
    """
    Функция высчитывающая натуральный логарифм 1 + x с точностью e
    """
    if check(x, min_val=-1, max_val=1) and check(e):
        result = 0
        current_n = 1
        current_member = 2

        while current_member > e:
            numerator = pow(x, current_n)
            denominator = factorial(current_n)
            current_member = numerator / denominator
            result += current_member * pow(-1, current_n + 1)
            current_n += 1
        return result
    else:
        return 'Incorrect values!'


def polynomial(x, e, a):
    """
    Функция высчитывающая 1 + x в степени a с точностью e
    """
    if check(x, min_val=-1, max_val=1) and check(e):
        result = 0
        current_n = 0
        current_member = 2
        prev_numerator = 1
        while current_member > e:
            if current_n == 0:
                current_member = 1
            else:
                numerator = (a - current_n + 1) * prev_numerator
                prev_numerator = numerator
                denominator = factorial(current_n)
                current_member = numerator / denominator * pow(x, current_n)
            result += current_member
            current_n += 1
        return result
    else:
        return 'Incorrect values!'


def arctg(x, e):
    """
    Функция высчитывающая арктангенс x с точностью e
    """
    if check(x, min_val=-1, max_val=1) and check(e):
        result = 0
        current_n = 0
        current_member = 2
        while current_member > e:
            numerator = pow(x, 2 * current_n + 1)
            denominator = 2 * current_n + 1
            current_member = numerator / denominator
            result += current_member * pow(-1, current_n)
            current_n += 1
        return result
    else:
        return 'Incorrect values!'
